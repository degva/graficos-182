#include <iostream>
#include <cstdlib>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include "../include/shader_utils.h"

using namespace std;

GLuint vbo_vertices;

// Identificador del shader:
// Se pueden tener tantos shaders sean necesarios
GLint program;

// Atributo Coordenadas
// Este atributo se linkea con un atributo del shader
GLint attribute_coord;

// Se crea la geometria, los shaders, y retorno un booleano
// true: si todo salio bien
// false: si errores
bool init_resources()
{
  // definimos la geometria
  // {x1,y1, x2,y2, x3,y3}
  GLfloat vertices[] = {1.0, 0.0, 0.0, 1.0, -1.0, 0.0};

  // crear uno o varios buffers
  // crea el identificador del buffer que se va a crear
  // aca no digo la cantidad de memoria que voy a asignar, el GPU te asigna
  // el espacio que calcula cuando se envia la data.
  glGenBuffers(1, &vbo_vertices);

  // Se indica que tipo de buffer es, en este caso: un arreglo de numeros
  glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);

  // Ahora se envia los datos al GPU
  // tipo de buffer, el tamano de la data, la data, y especificar si la data
  // que estoy enviando va a cambiar o no (si es dinamico o no)
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  GLint link_ok = GL_FALSE;

  // vector shader y el fragment shader
  GLuint vs, fs;

  // Definir los shaders
  if ((vs = create_shader("basic.v.glsl", GL_VERTEX_SHADER)) == 0)
    return false;
  if ((fs = create_shader("basic.f.glsl", GL_FRAGMENT_SHADER)) == 0)
    return false;

  // creamos el programa de shader y metemos ambos shaders
  // esto los compila, y los tiene como objectos en el GPU
  program = glCreateProgram();
  glAttachShader(program, vs);
  glAttachShader(program, fs);

  // Ahora linkeamos
  glLinkProgram(program);

  // vemos si todo okz
  // se pueden usar sufijo: f, i, c, f o 4f, 4i, 4c
  // i: integer, f: float, c: char
  glGetProgramiv(program, GL_LINK_STATUS, &link_ok);

  if (!link_ok)
  {
    cout << "El programa del shading tiene error" << endl;
    return false;
  }

  // lo que hace es, de este programa que se ha compilado arriba, quiero saber
  // donde se ubica donde esta esta variable del shader. Hay 2 formas para sacar
  // la variable del shader: una es con el nombre, o con el numero de llegada
  // se puede usar un diccionario para tener mayor control del meme
  attribute_coord = glGetAttribLocation(program, "coord");

  if (attribute_coord == -1)
  {
    cout << "Atributo coord no existe" << endl;
    return false;
  }

  return true;
}

void onDisplay()
{
  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(program);
  glEnableVertexAttribArray(attribute_coord);
  glBindBuffer(GL_ARRAY_BUFFER, vbo_vertices);

  // si se esta pasando valores como
  // {x1,x2,c1,c2,c3,x3,y3,c1,c2,c3} -> GL_TRUE
  // stride: seria cada cuanto, si se aplica lo de arriba, entonces seria cada 5
  // aparecen las coordenadas -> como el atributo color inicia en 2, -> 2,5
  glVertexAttribPointer(attribute_coord, 2, GL_FLOAT, GL_FALSE, 0, 0);

  // que grafico, en que coordenada inicio, y cuandos puntos tengo.
  glDrawArrays(GL_TRIANGLES, 0, 3);
  glDisableVertexAttribArray(attribute_coord);

  // intercambiamos los buffers, para el otro se envie y el que se esta
  // graficando se ponga disponible.
  glutSwapBuffers();
}

void free_resources()
{
  glDeleteProgram(program);
  glDeleteBuffers(1, &vbo_vertices);
}

int main(int argc, char *argv[])
{
  glutInit(&argc, argv);

  glutInitContextVersion(2, 0);
  glutInitContextProfile(GLUT_CORE_PROFILE);
  glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_ALPHA);
  glutInitWindowSize(400, 400);
  glutCreateWindow("Triangulo");

  GLenum glew_status = glewInit();
  if (glew_status != GLEW_OK)
  {
    cout << "Error con glew" << endl;
    exit(EXIT_FAILURE);
  }

  if (!GLEW_VERSION_2_0)
  {
    cout << "Tarjeta no soportada" << endl;
    exit(EXIT_FAILURE);
  }

  if (init_resources())
  {
    // registrar la funcion que va a hacer el dibujo
    glutDisplayFunc(onDisplay);
    // ahbilito unas banderas para el graficado
    glEnable(GL_BLEND); // para que se vea mas suave
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glutMainLoop();
  }
  else
  {
    return EXIT_FAILURE;
  }

  free_resources();
  return EXIT_SUCCESS;
}