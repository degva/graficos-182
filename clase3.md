# Clase 3 - Modelamiento Geometrico

## Representacion 3D

Existen varias formas de representar un objeto 3D:

- Representacion de frontera (B-rep, solo superficie). Se usa estructuras de
  datos para almacenar esto
- Geometria Constructiva de Solidos (CSG) se usan distintas figuras para crear
  un objeto mas complejo
- Volumetrica (Voxel) Es para representar informacion sobre el objeto. Se
  emplea mas en la representacion de fluidos.
- Quadtrees - Octrees (geometria computacional) son formas de guardar
- Particiones Binarias de Espacio (Geometria computacional) permiten hacer
  algoritmos eficientes geometricos. Para saber si un rayo de luz toca algo.

## Geoemtria Consutructiva
