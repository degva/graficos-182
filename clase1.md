# Clase 1

Dr. Ivan Sipiran

Graficado usando el CPU. Usaremos C++.

## Notas

- POV ray, un renderizador
- utiliza un algoritmo para realizar renderizado fotorealistico: **ray tracing**.
- POV ray usa un xml con la escena. (xml pues...)
- **Radiosity**: iluminacion global. Computacionalmente es muy intensivo.
- **Caustica** es cuando la luz es transportada por el vidrio. El algoritmo usado
  se llama Photo Mapping.
- Un framework para dibujo en la web: WebGL que utiliza el GPU, pero sobre este
  uno puede escribir en javascript. Hay una libreria llamada **three.js**
- se puede dividir en diferentes subcampos:
  - geometria 3d
  - ilumacion
  - cinematica: una cosa es que sea estatico y otra que sea en movimiento.
    interviene la fisica, matematica, etc
  - dinámica de fluidos: los fluidos tienen una forma de comportarse, distintas
    a como se comporta el resto. La forma como se simula, es a traves de un
    sistema de particulas.
- Para generar imagenes foto-realistas:
  el modelamiento -> escena -> realismo

## Aplicaciones

- Videojuegos
- Peliculas

## Este curso

### Objetivos

- conocer y aplicar conceptos de transformaciones 3D
- Estudiar algoritmos de proceamiento de color e ilumimacion
- algoritmos para hacer animaciones
- programar efectivamente los algoritmos en OpenGL haciendo uso de tecnicas de
  ultima generacion como shaders

### Estructura

- Semana 1: Vectores / Matrices
- Semana 2: OpenGL
- Semena 3: Representaciones B-Rep, mallas trianguales, estructuras de datos
- Semana 4: Vemos matematica, como generamos modelos a traves de funciones
  matematicas.
- Semana 5: Curvas y superficies NURBS
- Semana 6: Tranformaciones 3D
- Semana 7: Tranformacion de Vista II. Proyecciones.
- Semana 8: Procesamiento del color.
- Semana 9: Examen 1.
- Semana 10: Modelos de iluminacion. Implementacion en OpenGL
- Semana 11: Modelos de sombreado: Gouraud y Phong
- Semana 12: (Programamos un algoritmo desde 0) Iluminacion global y radiosidad
- Semana 13: Sistema de particulas
- Semana 14: Animacion cinematica: Angulos de Euler, cinematica hacia adelante
- Semana 15: Animacion cinematica: cinematica inversa
- Semana 16: Examen 2
