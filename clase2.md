# Clase 2

OpenGL es un estandar que indica como deben llamarse a algunas funciones para
realizar algunas funciones.
Son los fabricantes de hardware los que implementan tal version de OpenGL.

Cuando se revisar openGL, hay que ver la version. Cualquier version menor a
OpenGL < 2 no tienen el soporte directo del GPU.

Tesselation, es cuando una geometria se subdivide.
Rasterizacion, convertir a pixel

Vertex Specification > Vertex Shader > Tessellation > Geometry Shader >
Vertes Post-Processing > Primitive Assembly > Rasterization > Fragment Shader >
Per-sample Operations

Ademas de OpenGL, debemos usar el lenguage **GLSL** que se compila en el GPU.
Estos **shaders** (que se escribe con GLSL) debe ser bien pequeno.

Un programa tipico OpenGL ahora constara:

- Definicion de geometria
- Definicion de shaders
- Enviar geometria al GPU (hay que seguir un protocolo)
- Enlazar datos de nuestro programa y shader

## Geometria

La aplicacion y el GPU se hace mediante el uso de buffers. Almacenan propiedades
de los vertices.

Funciones: glGenBuffers (es como un malloc), glBufferData (para transmitir los
datos)

En el lenaguaje **GLSL** que las variables que estan definidas dentro del shader
se les conocen como atributos. Generalmente estos son datos que quiero que se
grafiquen.
